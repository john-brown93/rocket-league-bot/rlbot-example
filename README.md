# RLBotPythonExample
Example of a Python bot using the RLBot framework

## Quick Start
The easiest way to start a Python bot is demonstrated here!
https://youtu.be/YJ69QZ-EX7k

It shows you how to:
- Install the RLBot GUI
- Use it to create a new bot

## Changing the bot

- Bot behavior is controlled by `src/bot.py`
- Bot appearance is controlled by `src/appearance.cfg`

See https://github.com/RLBot/RLBotPythonExample/wiki for documentation and tutorials.

## Creating a new venv
- virtualenv rlbot --python=3.7
- Powershell activation: `./rlbot/Scripts/Activate.ps1`
- Ubuntu activation: `/rlbot/Scripts/Activate`

## Launch configurations for VSCode:
- Run RLBot and play against another bot:
```javascript
    {
        "name": "RLBot",
        "type": "python",
        "request": "launch",
        "program": "${workspaceFolder}/run.py",
        "console": "integratedTerminal"
    }
```
- Run RLBot and test against the training tasks:
```javascript
    {
        "name": "RLBot Unit tests",
        "type": "python",
        "request": "launch",
        "program": "${workspaceFolder}/training/unit_tests.py",
        "console": "integratedTerminal"
    }
```
