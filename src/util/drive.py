import math
from rlbot.agents.base_agent import SimpleControllerState
from rlbot.utils.structures.game_data_struct import PlayerInfo
from util.sequence import Sequence, ControlStep
from util.orientation import Orientation, relative_location
from util.vec import Vec3


def limit_to_safe_range(value: float) -> float:
    if value < -1:
        return -1
    if value > 1:
        return 1
    return value
    
def normalise_speed(value: float) -> float:
    """
    Depending on the distance to the ball, the bot should stop next to the ball
    """
    field_max_size = 10240
    min_distance = 90

    top = value - min_distance
    bottom = field_max_size - min_distance
    normalised_speed = (2 * abs(safe_divide(top, bottom))) - 1
    # print(f'normalised speed: {normalised_speed}')
    return limit_to_safe_range(normalised_speed)

def safe_divide(val1, val2):
    if val1 == 0:
        val1 = 0.001
    if val2 == 0:
        val2 = 0.001
    
    return val1/val2

def throttle(car: PlayerInfo, ball_location: Vec3) -> float:
    relative = relative_location(Vec3(car.physics.location), Orientation(car.physics.rotation), ball_location)
    distance = ball_location.dist(relative)
    # print(f'distance: {distance}')
    normalised_speed = normalise_speed(distance)
    return 1

def steer_toward_target(car: PlayerInfo, target: Vec3) -> float:
    relative = relative_location(Vec3(car.physics.location), Orientation(car.physics.rotation), target)
    angle = math.atan2(relative.y, relative.x)
    return [limit_to_safe_range(angle * 5), use_handbrake(angle)]

def orientate_to_ground(car: PlayerInfo) -> Sequence:
    is_off_ground = car.has_wheel_contact
    location = car.physics.location 
    if is_off_ground and location.z > 100: 
        return Sequence([
            ControlStep(duration=2, controls=SimpleControllerState(roll=-1)),
            ControlStep(duration=0.8, controls=SimpleControllerState())])
    return None

def use_handbrake(angle:float) -> bool:
    if angle > 1.3 or angle < -1.3:
        return True
    return False